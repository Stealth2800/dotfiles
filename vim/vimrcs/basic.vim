" VIM configuration file
" Bits and pieces taken from amix/vimrc on GitHub.
"
" TODO:
" - If not focused on NERDTree, but it's already visible, switch to it rather
"   than having to toggle it twice to get it to be focused.

" =============================================================================
" Install plugins
" =============================================================================
" TODO: Move this?
call plug#begin('~/.vim_plugged')

" Status bar
Plug 'itchyny/lightline.vim'
" File explorer
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
" Syntax checker
Plug 'w0rp/ale'

" Themes
"Plug 'blueshirts/darcula'
Plug 'drewtempelmeyer/palenight.vim'

" Languages
"if has("nvim")
"    Plug 'alaviss/nim.nvim', { 'for': 'nim' }
"else
"    Plug 'zah/nim.vim', { 'for': 'nim' }
"endif
Plug 'zah/nim.vim', { 'for': 'nim' }

call plug#end()


" =============================================================================
" General
" =============================================================================

" Lines of history for VIM
set history=500

" Performance booster
set lazyredraw

" Show matching brackets when highlighted by cursor
set showmatch
set mat=2

" Disable garbage
set nobackup
set nowb
set noswapfile


" =============================================================================
" Keybindings
" =============================================================================

let mapleader = ","
" Fast save
nmap <leader>w :w!<cr>

" Clear search highlight
map <silent> <leader><cr> :noh<cr>

" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Shift-tab = inverse tab
nnoremap <S-Tab> <<
inoremap <S-Tab> <C-d>

" Move a line of text using ALT+[jk] or Command+[jk] on mac
" TODO: Doesn't seem to work on Mac (iTerm)
nmap <M-j> mz:m+<cr>`z
nmap <M-k> mz:m-2<cr>`z
vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`z

if has("mac") || has("macunix")
  nmap <D-j> <M-j>
  nmap <D-k> <M-k>
  vmap <D-j> <M-j>
  vmap <D-k> <M-k>
endif


" =============================================================================
" Search
" =============================================================================

set wildmenu

" Ignore case when searching
set ignorecase
set smartcase
" Highlight search results
set hlsearch
set incsearch


" =============================================================================
" Appearance
" =============================================================================

" Line numbers
set number
set relativenumber

" Syntax highlighting
syntax enable

" Enable 256 colors palette in Gnome Terminal
if $COLORTERM == 'gnome-terminal'
    set t_Co=256
endif

try
    colorscheme palenight
catch
endtry

set background=dark

" Set extra options when running in GUI mode
if has("gui_running")
    set guioptions-=T
    set guioptions-=e
    set t_Co=256
    set guitablabel=%M\ %t
endif


" =============================================================================
" Editing behavior
" =============================================================================

" Spaces > tabs
set expandtab
set smarttab

" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4

set ai " Auto indent
set si " Smart indent

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" Use Unix as the standard file type
set ffs=unix,dos,mac

" Delete trailing white space on save, useful for some filetypes ;)
fun! CleanExtraSpaces()
    let save_cursor = getpos(".")
    let old_query = getreg('/')
    silent! %s/\s\+$//e
    call setpos('.', save_cursor)
    call setreg('/', old_query)
endfun

if has("autocmd")
    autocmd BufWritePre *.txt,*.js,*.py,*.wiki,*.sh,*.coffee :call CleanExtraSpaces()
endif

