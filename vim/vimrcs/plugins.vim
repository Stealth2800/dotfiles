" =============================================================================
" lightline
" =============================================================================
set laststatus=2

set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l\ \ Column:\ %c

" Returns true if paste mode is enabled
function! HasPaste()
    if &paste
        return 'PASTE MODE  '
    endif
    return ''
endfunction

" =============================================================================
" NERDTree
" =============================================================================
let g:NERDTreeWinPos = "left"
" Show hidden files
let NERDTreeShowHidden = 1
let g:NERDTreeWinSize = 35

" Auto close VIM if the last buffer is NERDTree
autocmd BufEnter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

map <leader>1 :NERDTreeToggle<cr>


